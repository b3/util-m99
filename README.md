# Simulateur de machine M99

[`m99`](m99) est un script bash offrant un simulateur pour la machine
M99. [`asm99`](asm99) est un *compilateur* de code assembleur M99 vers du
code machine M99.

La machine [M99], le processeur débranché, a été pensée et mise au point par
Martin QUINSON et Philippe MARQUET dans le cadre du projet [InfoSansOrdi].

[M99]: https://github.com/InfoSansOrdi/M999
[InfoSansOrdi]: https://github.com/InfoSansOrdi/InfoSansOrdi

Copyright (C) 2019 Bruno BEAUFILS

Les scripts distribués ici le sont sous les termes de la *licence WTFPL*
version 2, disponible dans le fichier [WTFPL-2](WTFPL-2).
